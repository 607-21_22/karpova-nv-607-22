document.addEventListener('DOMContentLoaded', function() {
    // Запросить список планов с сервера
    fetch('/get_plans')
    .then(response => response.json())
    .then(data => {
        data.forEach(plan => {
            addPlanToDOM(plan);
        });
    });

    // Обработчик формы добавления плана
    document.getElementById('addPlanForm').addEventListener('submit', function(event) {
        event.preventDefault();
        const planName = document.getElementById('planName').value;
        const planMonth = document.getElementById('planMonth').value;

        // Отправить данные на сервер
        fetch('/add_plan', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ name: planName, month: planMonth })
        })
        .then(response => response.json())
        .then(data => {
            addPlanToDOM(data);
            document.getElementById('planName').value = '';
            document.getElementById('planMonth').value = '';
        });
    });

    // Обработчик кнопки "выполнено"
    document.getElementById('plans').addEventListener('click', function(event) {
        if (event.target.classList.contains('completeButton')) {
            const planId = event.target.dataset.id;

            // Отправить запрос на сервер для удаления плана
            fetch(`/complete_plan/${planId}`, {
                method: 'PUT'
            })
            .then(response => response.json())
            .then(data => {
                event.target.parentElement.remove();
            });
        }
    });

    // Функция добавления плана на страницу
    function addPlanToDOM(plan) {
        const planElement = document.createElement('div');
        planElement.innerHTML = `<p>${plan.name}, Месяц: ${plan.month} <button class="completeButton" data-id="${plan.id}">Выполнено</button></p>`;
        document.getElementById('plans').appendChild(planElement);
    }
});