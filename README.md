# Karpova NV

## Планы на год

Этот проект представляет собой сайт, на котором пользователь может добавлять и отслеживать свои планы на год, разработанный с использованием технологий веб-разработки.


## Технологии

1) Фронтенд: React, HTML, CSS, JavaScript.
2) Бекенд: данные базы SQL (PostgreSQL).
3) Docker для контейнеризации приложений и баз данных
